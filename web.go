package main

import (
	"log"
	"net/http"
	"os"

	mqtt "github.com/eclipse/paho.mqtt.golang"

	"fmt"

	MQTTUtils "gitlab.com/NearTalk-MQTTServerGolang/mqtthandlers"
)

var listener mqtt.Client

func main() {
	done := make(chan bool)
	listener = MQTTUtils.ConnectListener()

	http.HandleFunc("/", handler)
	bind := fmt.Sprintf("%s:%s", os.Getenv("OPENSHIFT_GO_IP"), os.Getenv("OPENSHIFT_GO_PORT"))
	err := http.ListenAndServe(bind, nil)
	if err != nil {
		log.Fatal(err)
	}

	<-done
}

func handler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(fmt.Sprintf("The server is connected: %v\n", listener.IsConnected())))
}
