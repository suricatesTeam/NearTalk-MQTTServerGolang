package utils

// GlobalDestination The destination of the message for knowing that they are global
const GlobalDestination string = "global"

// ServerTopic the topic where the listener will subscribe
const ServerTopic string = "neartalk-mqtt"

// BrokerAddr The broker address
const BrokerAddr string = "tcp://broker.hivemq.com:1883"
