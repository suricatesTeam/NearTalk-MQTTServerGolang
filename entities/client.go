package entities

import (
	"regexp"
)

var validUUID = regexp.MustCompile(`^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$`)

// NTClient is the primary client of NearTalk
// it's used to store the clientID and their UUID
type NTClient struct {
	userID, clientUUID string
}

// CreateClient creates a client entity
func CreateClient(userID, clientUUID string) NTClient {
	var newClient NTClient
	if validUUID.MatchString(clientUUID) {
		newClient = NTClient{userID, clientUUID}
	}
	return newClient
}

// UserID gets the user id
func (c NTClient) UserID() string {
	return c.userID
}
