package entities

import (
	"encoding/json"
	"log"
)

// ZoneResponse is the interface
type ZoneResponse struct {
	NTZoneID    int64  `json:"ntzoneid"`
	Location    coords `json:"location"`
	Connections int32  `json:"connections"`
}

type coords struct {
	Coordinates []float32 `json:"coordinates"`
}

// DecodeZoneResponse gets the zone as an object from a json payload in bytes
func DecodeZoneResponse(payload []byte) (ZoneResponse, error) {
	var zoneResponse ZoneResponse
	err := json.Unmarshal(payload, &zoneResponse)
	if err != nil {
		log.Printf("Error decoding zone response: %s, %s\n", payload, err)
	}
	return zoneResponse, err
}
