package entities

import (
	"encoding/json"
	"log"
)

// Message is the interface of the main messages
type Message struct {
	Origin      string `json:"origin"`
	Destination string `json:"destination"`
	Message     string `json:"message"`
	Timestamp   int64  `json:"timestamp"`
	ClientID    string `json:"client_id"`
	ZoneID      string `json:"zone_id"`
	MessageID   string `json:"message_id"`
}

// DecodeMessage decodes a message from a json payload
func DecodeMessage(payload []byte) (Message, error) {
	var message Message
	err := json.Unmarshal(payload, &message)
	if err != nil {
		log.Println("Error decoding message:", payload, err)
	}
	return message, err
}

// EncodeMessage encodes a message to a json payload
func (message *Message) EncodeMessage() ([]byte, error) {
	encoded, err := json.Marshal(message)
	if err != nil {
		log.Printf("Error encoding message:", message, err)
	}
	return encoded, err
}
