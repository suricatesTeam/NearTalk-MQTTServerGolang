package mqtthandlers

import (
	"fmt"
	"time"

	"log"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	Entities "gitlab.com/NearTalk-MQTTServerGolang/entities"
	Utils "gitlab.com/NearTalk-MQTTServerGolang/utils"
)

var listenerClient MQTT.Client
var globalMessageBlocker = make(chan bool)
var blocked = false

//ConnectListener connects the global listener that is subscribed to the server topic
func ConnectListener() MQTT.Client {
	if listenerClient == nil || !listenerClient.IsConnected() {
		serverName := fmt.Sprintf("server_%v", time.Now().Unix())
		opts := MQTT.NewClientOptions().AddBroker(Utils.BrokerAddr)
		opts.SetClientID(serverName)
		opts.SetDefaultPublishHandler(messageHandler)
		opts.SetAutoReconnect(true)
		opts.SetCleanSession(true)
		opts.SetOnConnectHandler(func(MQTT.Client) {
			log.Println("Listener connected")
		})

		opts.SetConnectionLostHandler(clientConnectionLost)

		listenerClient = MQTT.NewClient(opts)

		if token := listenerClient.Connect(); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}

		if token := listenerClient.Subscribe(Utils.ServerTopic, 1, nil); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}

	return listenerClient
}

func clientConnectionLost(client MQTT.Client, err error) {
	if err != nil {
		log.Println("Connection of the listener lost:", err)
	}
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	log.Println("Listener restarted")
}

var messageHandler MQTT.MessageHandler = func(client MQTT.Client, mqttMessage MQTT.Message) {
	log.Printf("New msg: %s\n", string(mqttMessage.Payload()))

	decodedMessage, err := Entities.DecodeMessage(mqttMessage.Payload())
	if err == nil {
		if mqttMessage.Topic() == Utils.ServerTopic {
			go handleGlobalMessage(decodedMessage)
		}
	}
}
