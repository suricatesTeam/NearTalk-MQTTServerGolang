package mqtthandlers

import (
	"fmt"
	"log"
	"sync"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	Entities "gitlab.com/NearTalk-MQTTServerGolang/entities"
	Utils "gitlab.com/NearTalk-MQTTServerGolang/utils"
)

var redirectClientSingleton *MQTT.Client
var initOnce sync.Once
var clients = make(map[string]Entities.NTClient)

func handleGlobalMessage(m Entities.Message) {
	redirectGlobalMessage(&m, m.ZoneID)
}

func redirectGlobalMessage(m *Entities.Message, zoneTopic string) {
	m.Timestamp = time.Now().UnixNano() / 1000000
	encodedMessage, err := m.EncodeMessage()
	if err == nil {
		log.Printf("Sending global message %v to: %s ...\n", m, zoneTopic)
		c := *getRedirectClient()
		token := c.Publish(zoneTopic, 1, false, encodedMessage)
		if token.Wait() && token.Error() == nil {
			log.Println("Message sent!")
		} else {
			log.Println("Can't send message:", token.Error())
			log.Println("Retrying...")
			redirectGlobalMessage(m, zoneTopic)
		}
	} else {
		log.Printf("Can't encode global message: %v", m)
	}
}

func getRedirectClient() *MQTT.Client {
	if redirectClientSingleton != nil {
		var tempClient = *redirectClientSingleton
		if !tempClient.IsConnected() {
			initOnce.Do(func() {
				initSingleton()
			})
		}
	} else {
		initOnce.Do(func() {
			initSingleton()
		})
	}
	return redirectClientSingleton
}

func initSingleton() {
	serverName := fmt.Sprintf("redirect-client_%v", time.Now().Unix())
	opts := MQTT.NewClientOptions().AddBroker(Utils.BrokerAddr)
	opts.SetClientID(serverName)
	opts.SetAutoReconnect(true)
	opts.SetCleanSession(false)
	opts.SetOnConnectHandler(func(MQTT.Client) {
		log.Println("RedirectClient connected")
	})

	opts.SetConnectionLostHandler(redirectConnectionLost)

	var newClient = MQTT.NewClient(opts)

	if token := newClient.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	redirectClientSingleton = &newClient
}

func redirectConnectionLost(client MQTT.Client, err error) {
	if err != nil {
		log.Println("Connection of the redirect client lost:", err)
	}
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	log.Println("Redirect client restarted")
}
